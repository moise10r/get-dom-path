# get element path in the dom

- Javascript
- Html

## Getting Started

**To get the local copy of the project up and running you will need to run the following commands on your terminal:**

`git clone git@gitlab.com:moise10r/get-dom-path.git`

To install all the available dependecies run:
`npm install`

To run this application locally :
`npm run dev`
And then open your browser on the port 3000


## Author

👤 NGANULO RUSHANIKA Moise

- GitHub: [@githubhandle](https://github.com/moise10r)
- Twitter: [@twitterhandle](https://twitter.com/MRushanika)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/nganulo-rushanika-mo%C3%AFse-626139197/)


## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://github.com/moise10r/Awesome_Book/issues).

## Show your support

Give a ⭐️ if you like this project!❤️❤️❤️