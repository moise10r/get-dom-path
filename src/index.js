import domElementPath from 'dom-element-path';

document.addEventListener('click', function(e) {
    const clickedElementPath = domElementPath(e.target);
    const clickedElementPathWithoutDocument = clickedElementPath.slice(24);
    console.log(clickedElementPathWithoutDocument);
    const clickedElementPathArray = clickedElementPathWithoutDocument.split('>');
    console.log(clickedElementPathArray);
}, false);